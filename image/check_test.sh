#!/bin/bash

set -e

while  [ ! -f  "/test/done" ]; do echo "Waiting for tests to finish"; sleep 1; done

PNAME="${1}"
echo "Tests are done, stopping process [${PNAME}]"
pkill -f ${PNAME}